<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1eb7daff-800f-4443-b3f8-a8650e879d8e(jetbrains.mps.grammarLanguage.adaptation.plugin)">
  <persistence version="9" />
  <languages>
    <use id="28f9e497-3b42-4291-aeba-0a1039153ab1" name="jetbrains.mps.lang.plugin" version="4" />
    <use id="ef7bf5ac-d06c-4342-b11d-e42104eb9343" name="jetbrains.mps.lang.plugin.standalone" version="0" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="17" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="9" />
  </languages>
  <imports>
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="lc8y" ref="ca9e71ec-ac38-418e-8818-467c94563fd1/java:jetbrains.mps.grammarLanguage.grammar(jetbrains.mps.grammarLanguage.adaptation/)" />
    <import index="c8s6" ref="r:08f8613a-9ff2-487d-bf55-5ceca2c577dd(jetbrains.mps.samples.grammarLanguage.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="tprs" ref="r:00000000-0000-4000-0000-011c895904a4(jetbrains.mps.ide.actions)" implicit="true" />
  </imports>
  <registry>
    <language id="28f9e497-3b42-4291-aeba-0a1039153ab1" name="jetbrains.mps.lang.plugin">
      <concept id="1207145163717" name="jetbrains.mps.lang.plugin.structure.ElementListContents" flags="ng" index="ftmFs">
        <child id="1207145201301" name="reference" index="ftvYc" />
      </concept>
      <concept id="1203071646776" name="jetbrains.mps.lang.plugin.structure.ActionDeclaration" flags="ng" index="sE7Ow">
        <property id="1205250923097" name="caption" index="2uzpH1" />
        <property id="1213273179528" name="description" index="1WHSii" />
        <child id="1203083461638" name="executeFunction" index="tncku" />
        <child id="1217413222820" name="parameter" index="1NuT2Z" />
      </concept>
      <concept id="1203083511112" name="jetbrains.mps.lang.plugin.structure.ExecuteBlock" flags="in" index="tnohg" />
      <concept id="1203087890642" name="jetbrains.mps.lang.plugin.structure.ActionGroupDeclaration" flags="ng" index="tC5Ba">
        <child id="1204991552650" name="modifier" index="2f5YQi" />
        <child id="1207145245948" name="contents" index="ftER_" />
      </concept>
      <concept id="1203088046679" name="jetbrains.mps.lang.plugin.structure.ActionInstance" flags="ng" index="tCFHf">
        <reference id="1203088061055" name="action" index="tCJdB" />
      </concept>
      <concept id="1203092361741" name="jetbrains.mps.lang.plugin.structure.ModificationStatement" flags="lg" index="tT9cl">
        <reference id="1203092736097" name="modifiedGroup" index="tU$_T" />
      </concept>
      <concept id="1205679047295" name="jetbrains.mps.lang.plugin.structure.ActionParameterDeclaration" flags="ig" index="2S4$dB" />
      <concept id="1206092561075" name="jetbrains.mps.lang.plugin.structure.ActionParameterReferenceOperation" flags="nn" index="3gHZIF" />
      <concept id="5538333046911348654" name="jetbrains.mps.lang.plugin.structure.RequiredCondition" flags="ng" index="1oajcY" />
      <concept id="1217413147516" name="jetbrains.mps.lang.plugin.structure.ActionParameter" flags="ng" index="1NuADB">
        <child id="5538333046911298738" name="condition" index="1oa70y" />
      </concept>
    </language>
    <language id="ef7bf5ac-d06c-4342-b11d-e42104eb9343" name="jetbrains.mps.lang.plugin.standalone">
      <concept id="7520713872864775836" name="jetbrains.mps.lang.plugin.standalone.structure.StandalonePluginDescriptor" flags="ng" index="2DaZZR" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081256982272" name="jetbrains.mps.baseLanguage.structure.InstanceOfExpression" flags="nn" index="2ZW3vV">
        <child id="1081256993305" name="classType" index="2ZW6by" />
        <child id="1081256993304" name="leftExpression" index="2ZW6bz" />
      </concept>
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="1350122676458893092" name="text" index="3ndbpf" />
      </concept>
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers">
      <concept id="1205752633985" name="jetbrains.mps.baseLanguage.classifiers.structure.ThisClassifierExpression" flags="nn" index="2WthIp" />
      <concept id="1205756064662" name="jetbrains.mps.baseLanguage.classifiers.structure.IMemberOperation" flags="ng" index="2WEnae">
        <reference id="1205756909548" name="member" index="2WH_rO" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1181949435690" name="jetbrains.mps.lang.smodel.structure.Concept_NewInstance" flags="nn" index="LFhST" />
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1154546950173" name="jetbrains.mps.lang.smodel.structure.ConceptReference" flags="ng" index="3gn64h">
        <reference id="1154546997487" name="concept" index="3gnhBz" />
      </concept>
      <concept id="5944356402132808749" name="jetbrains.mps.lang.smodel.structure.ConceptSwitchStatement" flags="nn" index="1_3QMa">
        <child id="5944356402132808753" name="case" index="1_3QMm" />
        <child id="5944356402132808752" name="expression" index="1_3QMn" />
      </concept>
      <concept id="5944356402132808754" name="jetbrains.mps.lang.smodel.structure.SubconceptCase" flags="ng" index="1_3QMl">
        <child id="1163670677455" name="concept" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="c7fb639f-be78-4307-89b0-b5959c3fa8c8" name="jetbrains.mps.lang.text">
      <concept id="155656958578482948" name="jetbrains.mps.lang.text.structure.Word" flags="ng" index="3oM_SD">
        <property id="155656958578482949" name="value" index="3oM_SC" />
      </concept>
      <concept id="2535923850359271782" name="jetbrains.mps.lang.text.structure.Line" flags="ng" index="1PaTwC">
        <child id="2535923850359271783" name="elements" index="1PaTwD" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1227022210526" name="jetbrains.mps.baseLanguage.collections.structure.ClearAllElementsOperation" flags="nn" index="2Kehj3" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
    </language>
  </registry>
  <node concept="2DaZZR" id="6cQ5Sd6nOiM" />
  <node concept="sE7Ow" id="6cQ5Sd6nOiN">
    <property role="TrG5h" value="adaptGrammar" />
    <property role="2uzpH1" value="Adapt Grammar" />
    <property role="1WHSii" value="Generate additional rules and transformations" />
    <node concept="tnohg" id="6cQ5Sd6nOiO" role="tncku">
      <node concept="3clFbS" id="6cQ5Sd6nOiP" role="2VODD2">
        <node concept="3SKdUt" id="7vtgdGlV$zv" role="3cqZAp">
          <node concept="1PaTwC" id="7vtgdGlV$zw" role="3ndbpf">
            <node concept="3oM_SD" id="7vtgdGlV$zy" role="1PaTwD">
              <property role="3oM_SC" value="Clear" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVBCt" role="1PaTwD">
              <property role="3oM_SC" value="old" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVBCJ" role="1PaTwD">
              <property role="3oM_SC" value="derived" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVBDe" role="1PaTwD">
              <property role="3oM_SC" value="info" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="6cQ5Sd6pr9p" role="3cqZAp">
          <node concept="2OqwBi" id="6cQ5Sd6pDVa" role="3clFbG">
            <node concept="2OqwBi" id="6cQ5Sd6prYE" role="2Oq$k0">
              <node concept="2OqwBi" id="6cQ5Sd6prlz" role="2Oq$k0">
                <node concept="2WthIp" id="6cQ5Sd6pr9n" role="2Oq$k0" />
                <node concept="3gHZIF" id="6cQ5Sd6pr_O" role="2OqNvi">
                  <ref role="2WH_rO" node="6cQ5Sd6oWpT" resolve="node" />
                </node>
              </node>
              <node concept="3Tsc0h" id="6cQ5Sd6psdP" role="2OqNvi">
                <ref role="3TtcxE" to="c8s6:4vv0wgOalpj" resolve="derivedInfo" />
              </node>
            </node>
            <node concept="2Kehj3" id="6cQ5Sd6pHAL" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbH" id="7vtgdGlVBDI" role="3cqZAp" />
        <node concept="3SKdUt" id="7vtgdGlVIl9" role="3cqZAp">
          <node concept="1PaTwC" id="7vtgdGlVIla" role="3ndbpf">
            <node concept="3oM_SD" id="7vtgdGlVIlc" role="1PaTwD">
              <property role="3oM_SC" value="Map" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVLAC" role="1PaTwD">
              <property role="3oM_SC" value="grammar" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVLB6" role="1PaTwD">
              <property role="3oM_SC" value="to" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVLB_" role="1PaTwD">
              <property role="3oM_SC" value="java" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVLBT" role="1PaTwD">
              <property role="3oM_SC" value="classes" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7vtgdGlTBji" role="3cqZAp">
          <node concept="3cpWsn" id="7vtgdGlTBjo" role="3cpWs9">
            <property role="TrG5h" value="decByName" />
            <node concept="3uibUv" id="7vtgdGlTBjq" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~HashMap" resolve="HashMap" />
              <node concept="17QB3L" id="7vtgdGlTEbh" role="11_B2D" />
              <node concept="3Tqbb2" id="7vtgdGlTEgp" role="11_B2D">
                <ref role="ehGHo" to="c8s6:4vv0wgO7VEI" resolve="SymbolDeclaration" />
              </node>
            </node>
            <node concept="2ShNRf" id="7vtgdGlTEiI" role="33vP2m">
              <node concept="1pGfFk" id="7vtgdGlTEpt" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~HashMap.&lt;init&gt;()" resolve="HashMap" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7vtgdGlOeDB" role="3cqZAp">
          <node concept="3cpWsn" id="7vtgdGlOeDA" role="3cpWs9">
            <property role="TrG5h" value="rules" />
            <node concept="3uibUv" id="7vtgdGlOeDC" role="1tU5fm">
              <ref role="3uigEE" to="33ny:~ArrayList" resolve="ArrayList" />
              <node concept="3uibUv" id="7vtgdGlOeDD" role="11_B2D">
                <ref role="3uigEE" to="lc8y:~GrammarRule" resolve="GrammarRule" />
              </node>
            </node>
            <node concept="2ShNRf" id="7vtgdGlOgZL" role="33vP2m">
              <node concept="1pGfFk" id="7vtgdGlOh6p" role="2ShVmc">
                <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="3EBZBpjS0z0" role="3cqZAp">
          <node concept="2GrKxI" id="3EBZBpjS0z2" role="2Gsz3X">
            <property role="TrG5h" value="rule" />
          </node>
          <node concept="2OqwBi" id="3EBZBpjS54C" role="2GsD0m">
            <node concept="2OqwBi" id="3EBZBpjS4VF" role="2Oq$k0">
              <node concept="2WthIp" id="3EBZBpjS4VI" role="2Oq$k0" />
              <node concept="3gHZIF" id="3EBZBpjS4VK" role="2OqNvi">
                <ref role="2WH_rO" node="6cQ5Sd6oWpT" resolve="node" />
              </node>
            </node>
            <node concept="3Tsc0h" id="3EBZBpjS5jK" role="2OqNvi">
              <ref role="3TtcxE" to="c8s6:4vv0wgO7CcX" resolve="rules" />
            </node>
          </node>
          <node concept="3clFbS" id="3EBZBpjS0z6" role="2LFqv$">
            <node concept="3cpWs8" id="7vtgdGlN$Fy" role="3cqZAp">
              <node concept="3cpWsn" id="7vtgdGlN$Fz" role="3cpWs9">
                <property role="TrG5h" value="from" />
                <node concept="3uibUv" id="7vtgdGlN$F$" role="1tU5fm">
                  <ref role="3uigEE" to="lc8y:~NonTerminal" resolve="NonTerminal" />
                </node>
                <node concept="2ShNRf" id="7vtgdGlN$YR" role="33vP2m">
                  <node concept="1pGfFk" id="7vtgdGlN$YS" role="2ShVmc">
                    <ref role="37wK5l" to="lc8y:~NonTerminal(String)" resolve="NonTerminal" />
                    <node concept="2OqwBi" id="7vtgdGlNC3h" role="37wK5m">
                      <node concept="2OqwBi" id="7vtgdGlRwdW" role="2Oq$k0">
                        <node concept="2OqwBi" id="7vtgdGlNA2s" role="2Oq$k0">
                          <node concept="2GrUjf" id="7vtgdGlN_MA" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="3EBZBpjS0z2" resolve="rule" />
                          </node>
                          <node concept="3TrEf2" id="7vtgdGlNAEA" role="2OqNvi">
                            <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cai" resolve="leftHandSide" />
                          </node>
                        </node>
                        <node concept="3TrEf2" id="7vtgdGlRwF1" role="2OqNvi">
                          <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                        </node>
                      </node>
                      <node concept="3TrcHB" id="7vtgdGlNCup" role="2OqNvi">
                        <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="7vtgdGlN7_G" role="3cqZAp">
              <node concept="3cpWsn" id="7vtgdGlN7_M" role="3cpWs9">
                <property role="TrG5h" value="to" />
                <node concept="3uibUv" id="7vtgdGlN7_O" role="1tU5fm">
                  <ref role="3uigEE" to="33ny:~List" resolve="List" />
                  <node concept="3uibUv" id="7vtgdGlN7Hw" role="11_B2D">
                    <ref role="3uigEE" to="lc8y:~Symbol" resolve="Symbol" />
                  </node>
                </node>
                <node concept="2ShNRf" id="7vtgdGlN7IJ" role="33vP2m">
                  <node concept="1pGfFk" id="7vtgdGlN7Pn" role="2ShVmc">
                    <ref role="37wK5l" to="33ny:~ArrayList.&lt;init&gt;()" resolve="ArrayList" />
                    <node concept="3uibUv" id="7vtgdGlN88U" role="1pMfVU">
                      <ref role="3uigEE" to="lc8y:~Symbol" resolve="Symbol" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2Gpval" id="7vtgdGlN8_m" role="3cqZAp">
              <node concept="2GrKxI" id="7vtgdGlN8_o" role="2Gsz3X">
                <property role="TrG5h" value="symb" />
              </node>
              <node concept="2OqwBi" id="7vtgdGlN8Mw" role="2GsD0m">
                <node concept="2GrUjf" id="7vtgdGlN8Em" role="2Oq$k0">
                  <ref role="2Gs0qQ" node="3EBZBpjS0z2" resolve="rule" />
                </node>
                <node concept="3Tsc0h" id="7vtgdGlN9ct" role="2OqNvi">
                  <ref role="3TtcxE" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
                </node>
              </node>
              <node concept="3clFbS" id="7vtgdGlN8_s" role="2LFqv$">
                <node concept="1_3QMa" id="7vtgdGlNdiW" role="3cqZAp">
                  <node concept="1_3QMl" id="7vtgdGlNdGY" role="1_3QMm">
                    <node concept="3gn64h" id="7vtgdGlNdHm" role="3Kbmr1">
                      <ref role="3gnhBz" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
                    </node>
                    <node concept="3clFbS" id="7vtgdGlNdH2" role="3Kbo56">
                      <node concept="3cpWs8" id="7vtgdGlTF8Q" role="3cqZAp">
                        <node concept="3cpWsn" id="7vtgdGlTF8T" role="3cpWs9">
                          <property role="TrG5h" value="dec" />
                          <node concept="3Tqbb2" id="7vtgdGlTF8O" role="1tU5fm">
                            <ref role="ehGHo" to="c8s6:4vv0wgO7Ca8" resolve="TerminalDeclaration" />
                          </node>
                          <node concept="2OqwBi" id="7vtgdGlTG$O" role="33vP2m">
                            <node concept="1PxgMI" id="7vtgdGlTF_F" role="2Oq$k0">
                              <node concept="chp4Y" id="7vtgdGlTG9_" role="3oSUPX">
                                <ref role="cht4Q" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
                              </node>
                              <node concept="2GrUjf" id="7vtgdGlTFtX" role="1m5AlR">
                                <ref role="2Gs0qQ" node="7vtgdGlN8_o" resolve="symb" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="7vtgdGlTHmt" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cad" resolve="declaration" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="7vtgdGlUdbB" role="3cqZAp">
                        <node concept="2OqwBi" id="7vtgdGlUdwV" role="3clFbG">
                          <node concept="37vLTw" id="7vtgdGlUdwU" role="2Oq$k0">
                            <ref role="3cqZAo" node="7vtgdGlTBjo" resolve="decByName" />
                          </node>
                          <node concept="liA8E" id="7vtgdGlUdwW" role="2OqNvi">
                            <ref role="37wK5l" to="33ny:~HashMap.put(java.lang.Object,java.lang.Object)" resolve="put" />
                            <node concept="2OqwBi" id="7vtgdGlUfKA" role="37wK5m">
                              <node concept="37vLTw" id="7vtgdGlUdwY" role="2Oq$k0">
                                <ref role="3cqZAo" node="7vtgdGlTF8T" resolve="dec" />
                              </node>
                              <node concept="3TrcHB" id="7vtgdGlUgaA" role="2OqNvi">
                                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                              </node>
                            </node>
                            <node concept="37vLTw" id="7vtgdGlUdx8" role="37wK5m">
                              <ref role="3cqZAo" node="7vtgdGlTF8T" resolve="dec" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="7vtgdGlNdMs" role="3cqZAp">
                        <node concept="2OqwBi" id="7vtgdGlNebU" role="3clFbG">
                          <node concept="37vLTw" id="7vtgdGlNdMr" role="2Oq$k0">
                            <ref role="3cqZAo" node="7vtgdGlN7_M" resolve="to" />
                          </node>
                          <node concept="liA8E" id="7vtgdGlNeWm" role="2OqNvi">
                            <ref role="37wK5l" to="33ny:~List.add(java.lang.Object)" resolve="add" />
                            <node concept="2ShNRf" id="7vtgdGlNr97" role="37wK5m">
                              <node concept="1pGfFk" id="7vtgdGlNr98" role="2ShVmc">
                                <ref role="37wK5l" to="lc8y:~Terminal(String)" resolve="Terminal" />
                                <node concept="2OqwBi" id="7vtgdGlRxqK" role="37wK5m">
                                  <node concept="3TrcHB" id="7vtgdGlRycP" role="2OqNvi">
                                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                  </node>
                                  <node concept="37vLTw" id="7vtgdGlUgUd" role="2Oq$k0">
                                    <ref role="3cqZAo" node="7vtgdGlTF8T" resolve="dec" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="7vtgdGlNdjH" role="1_3QMn">
                    <node concept="2GrUjf" id="7vtgdGlNdjl" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="7vtgdGlN8_o" resolve="symb" />
                    </node>
                    <node concept="2yIwOk" id="7vtgdGlNdG8" role="2OqNvi" />
                  </node>
                  <node concept="1_3QMl" id="7vtgdGlNdIs" role="1_3QMm">
                    <node concept="3gn64h" id="7vtgdGlNdIR" role="3Kbmr1">
                      <ref role="3gnhBz" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                    </node>
                    <node concept="3clFbS" id="7vtgdGlNdIw" role="3Kbo56">
                      <node concept="3cpWs8" id="7vtgdGlUhBt" role="3cqZAp">
                        <node concept="3cpWsn" id="7vtgdGlUhBu" role="3cpWs9">
                          <property role="TrG5h" value="dec" />
                          <node concept="3Tqbb2" id="7vtgdGlUhBv" role="1tU5fm">
                            <ref role="ehGHo" to="c8s6:4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
                          </node>
                          <node concept="2OqwBi" id="7vtgdGlUla4" role="33vP2m">
                            <node concept="1PxgMI" id="7vtgdGlUhBx" role="2Oq$k0">
                              <node concept="chp4Y" id="7vtgdGlUiFF" role="3oSUPX">
                                <ref role="cht4Q" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                              </node>
                              <node concept="2GrUjf" id="7vtgdGlUhBz" role="1m5AlR">
                                <ref role="2Gs0qQ" node="7vtgdGlN8_o" resolve="symb" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="7vtgdGlUm8I" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="7vtgdGlUhB_" role="3cqZAp">
                        <node concept="2OqwBi" id="7vtgdGlUhBA" role="3clFbG">
                          <node concept="37vLTw" id="7vtgdGlUhBB" role="2Oq$k0">
                            <ref role="3cqZAo" node="7vtgdGlTBjo" resolve="decByName" />
                          </node>
                          <node concept="liA8E" id="7vtgdGlUhBC" role="2OqNvi">
                            <ref role="37wK5l" to="33ny:~HashMap.put(java.lang.Object,java.lang.Object)" resolve="put" />
                            <node concept="2OqwBi" id="7vtgdGlUhBD" role="37wK5m">
                              <node concept="37vLTw" id="7vtgdGlUhBE" role="2Oq$k0">
                                <ref role="3cqZAo" node="7vtgdGlUhBu" resolve="dec" />
                              </node>
                              <node concept="3TrcHB" id="7vtgdGlUhBF" role="2OqNvi">
                                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                              </node>
                            </node>
                            <node concept="37vLTw" id="7vtgdGlUhBG" role="37wK5m">
                              <ref role="3cqZAo" node="7vtgdGlUhBu" resolve="dec" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="7vtgdGlUhBH" role="3cqZAp">
                        <node concept="2OqwBi" id="7vtgdGlUhBI" role="3clFbG">
                          <node concept="37vLTw" id="7vtgdGlUhBJ" role="2Oq$k0">
                            <ref role="3cqZAo" node="7vtgdGlN7_M" resolve="to" />
                          </node>
                          <node concept="liA8E" id="7vtgdGlUhBK" role="2OqNvi">
                            <ref role="37wK5l" to="33ny:~List.add(java.lang.Object)" resolve="add" />
                            <node concept="2ShNRf" id="7vtgdGlUhBL" role="37wK5m">
                              <node concept="1pGfFk" id="7vtgdGlUhBM" role="2ShVmc">
                                <ref role="37wK5l" to="lc8y:~Terminal(String)" resolve="Terminal" />
                                <node concept="2OqwBi" id="7vtgdGlUhBN" role="37wK5m">
                                  <node concept="3TrcHB" id="7vtgdGlUhBO" role="2OqNvi">
                                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                                  </node>
                                  <node concept="37vLTw" id="7vtgdGlUhBP" role="2Oq$k0">
                                    <ref role="3cqZAo" node="7vtgdGlUhBu" resolve="dec" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7vtgdGlOhAV" role="3cqZAp">
              <node concept="2OqwBi" id="7vtgdGlOixn" role="3clFbG">
                <node concept="37vLTw" id="7vtgdGlOhAT" role="2Oq$k0">
                  <ref role="3cqZAo" node="7vtgdGlOeDA" resolve="rules" />
                </node>
                <node concept="liA8E" id="7vtgdGlOk9X" role="2OqNvi">
                  <ref role="37wK5l" to="33ny:~ArrayList.add(java.lang.Object)" resolve="add" />
                  <node concept="2ShNRf" id="7vtgdGlOkl2" role="37wK5m">
                    <node concept="1pGfFk" id="7vtgdGlOkl3" role="2ShVmc">
                      <ref role="37wK5l" to="lc8y:~GrammarRule(String,NonTerminal,List)" resolve="GrammarRule" />
                      <node concept="2OqwBi" id="7vtgdGlOkl4" role="37wK5m">
                        <node concept="2GrUjf" id="7vtgdGlOkl5" role="2Oq$k0">
                          <ref role="2Gs0qQ" node="3EBZBpjS0z2" resolve="rule" />
                        </node>
                        <node concept="3TrcHB" id="7vtgdGlOkl6" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="7vtgdGlOkl7" role="37wK5m">
                        <ref role="3cqZAo" node="7vtgdGlN$Fz" resolve="from" />
                      </node>
                      <node concept="37vLTw" id="7vtgdGlOkl8" role="37wK5m">
                        <ref role="3cqZAo" node="7vtgdGlN7_M" resolve="to" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7vtgdGlVSqn" role="3cqZAp">
          <node concept="3cpWsn" id="7vtgdGlVSqo" role="3cpWs9">
            <property role="TrG5h" value="g" />
            <node concept="3uibUv" id="7vtgdGlVSqp" role="1tU5fm">
              <ref role="3uigEE" to="lc8y:~Grammar" resolve="Grammar" />
            </node>
            <node concept="2ShNRf" id="7vtgdGlVSqq" role="33vP2m">
              <node concept="1pGfFk" id="7vtgdGlVSqr" role="2ShVmc">
                <ref role="37wK5l" to="lc8y:~Grammar(List)" resolve="Grammar" />
                <node concept="37vLTw" id="7vtgdGlVSqs" role="37wK5m">
                  <ref role="3cqZAo" node="7vtgdGlOeDA" resolve="rules" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7vtgdGlXrsz" role="3cqZAp" />
        <node concept="3SKdUt" id="7vtgdGlVOYa" role="3cqZAp">
          <node concept="1PaTwC" id="7vtgdGlVOYb" role="3ndbpf">
            <node concept="3oM_SD" id="7vtgdGlVOYd" role="1PaTwD">
              <property role="3oM_SC" value="Adapt" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlVSpH" role="1PaTwD">
              <property role="3oM_SC" value="grammar" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="3iSkm4L6aer" role="3cqZAp">
          <node concept="2OqwBi" id="3iSkm4L7fSb" role="3clFbG">
            <node concept="37vLTw" id="3iSkm4L6aep" role="2Oq$k0">
              <ref role="3cqZAo" node="7vtgdGlVSqo" resolve="g" />
            </node>
            <node concept="liA8E" id="3iSkm4L7gMA" role="2OqNvi">
              <ref role="37wK5l" to=":^" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7vtgdGlSCvN" role="3cqZAp" />
        <node concept="3SKdUt" id="7vtgdGlW3fg" role="3cqZAp">
          <node concept="1PaTwC" id="7vtgdGlW3fh" role="3ndbpf">
            <node concept="3oM_SD" id="7vtgdGlW3fj" role="1PaTwD">
              <property role="3oM_SC" value="Build" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlW6IU" role="1PaTwD">
              <property role="3oM_SC" value="additional" />
            </node>
            <node concept="3oM_SD" id="7vtgdGlW6Jo" role="1PaTwD">
              <property role="3oM_SC" value="rules" />
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="7vtgdGlR1g8" role="3cqZAp">
          <node concept="3cpWsn" id="7vtgdGlR1gb" role="3cpWs9">
            <property role="TrG5h" value="table" />
            <node concept="3Tqbb2" id="7vtgdGlR1g6" role="1tU5fm">
              <ref role="ehGHo" to="c8s6:3v$U6ONNksH" resolve="AdditionalRulesTable" />
            </node>
            <node concept="2OqwBi" id="7vtgdGlR45N" role="33vP2m">
              <node concept="35c_gC" id="7vtgdGlR3Mz" role="2Oq$k0">
                <ref role="35c_gD" to="c8s6:3v$U6ONNksH" resolve="AdditionalRulesTable" />
              </node>
              <node concept="LFhST" id="7vtgdGlR4u4" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="2Gpval" id="7vtgdGlPEVe" role="3cqZAp">
          <node concept="2GrKxI" id="7vtgdGlPEVg" role="2Gsz3X">
            <property role="TrG5h" value="rule" />
          </node>
          <node concept="3clFbS" id="7vtgdGlPEVk" role="2LFqv$">
            <node concept="3cpWs8" id="7vtgdGlRjkA" role="3cqZAp">
              <node concept="3cpWsn" id="7vtgdGlRjkD" role="3cpWs9">
                <property role="TrG5h" value="newRule" />
                <node concept="3Tqbb2" id="7vtgdGlRjk_" role="1tU5fm">
                  <ref role="ehGHo" to="c8s6:4vv0wgO7Ca7" resolve="GrammarRule" />
                </node>
                <node concept="2OqwBi" id="7vtgdGlRjE2" role="33vP2m">
                  <node concept="35c_gC" id="7vtgdGlRjmM" role="2Oq$k0">
                    <ref role="35c_gD" to="c8s6:4vv0wgO7Ca7" resolve="GrammarRule" />
                  </node>
                  <node concept="LFhST" id="7vtgdGlRk2l" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7vtgdGlRUGP" role="3cqZAp">
              <node concept="37vLTI" id="7vtgdGlRVDl" role="3clFbG">
                <node concept="2OqwBi" id="3iSkm4L7jxG" role="37vLTx">
                  <node concept="2GrUjf" id="7vtgdGlRW5c" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="7vtgdGlPEVg" resolve="rule" />
                  </node>
                  <node concept="liA8E" id="3iSkm4L7ktK" role="2OqNvi">
                    <ref role="37wK5l" to=":^" />
                  </node>
                </node>
                <node concept="2OqwBi" id="7vtgdGlRUOY" role="37vLTJ">
                  <node concept="37vLTw" id="7vtgdGlRUGN" role="2Oq$k0">
                    <ref role="3cqZAo" node="7vtgdGlRjkD" resolve="newRule" />
                  </node>
                  <node concept="3TrcHB" id="7vtgdGlRV1W" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7vtgdGlUq9Q" role="3cqZAp">
              <node concept="37vLTI" id="7vtgdGlUr19" role="3clFbG">
                <node concept="2OqwBi" id="7vtgdGlUryA" role="37vLTx">
                  <node concept="35c_gC" id="7vtgdGlUr7_" role="2Oq$k0">
                    <ref role="35c_gD" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                  </node>
                  <node concept="LFhST" id="7vtgdGlUrWP" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="7vtgdGlUqtz" role="37vLTJ">
                  <node concept="37vLTw" id="7vtgdGlUq9O" role="2Oq$k0">
                    <ref role="3cqZAo" node="7vtgdGlRjkD" resolve="newRule" />
                  </node>
                  <node concept="3TrEf2" id="7vtgdGlUqGA" role="2OqNvi">
                    <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cai" resolve="leftHandSide" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7vtgdGlUsmZ" role="3cqZAp">
              <node concept="37vLTI" id="7vtgdGlUtSJ" role="3clFbG">
                <node concept="10QFUN" id="7vtgdGlU$KL" role="37vLTx">
                  <node concept="2OqwBi" id="7vtgdGlUuC5" role="10QFUP">
                    <node concept="37vLTw" id="7vtgdGlUtZG" role="2Oq$k0">
                      <ref role="3cqZAo" node="7vtgdGlTBjo" resolve="decByName" />
                    </node>
                    <node concept="liA8E" id="7vtgdGlUvMR" role="2OqNvi">
                      <ref role="37wK5l" to="33ny:~HashMap.get(java.lang.Object)" resolve="get" />
                      <node concept="2OqwBi" id="3iSkm4L7r0d" role="37wK5m">
                        <node concept="2OqwBi" id="3iSkm4L7oMf" role="2Oq$k0">
                          <node concept="2GrUjf" id="3iSkm4L7ohM" role="2Oq$k0">
                            <ref role="2Gs0qQ" node="7vtgdGlPEVg" resolve="rule" />
                          </node>
                          <node concept="liA8E" id="3iSkm4L7q5_" role="2OqNvi">
                            <ref role="37wK5l" to=":^" />
                          </node>
                        </node>
                        <node concept="liA8E" id="3iSkm4L7s5m" role="2OqNvi">
                          <ref role="37wK5l" to=":^" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3Tqbb2" id="7vtgdGlU$KM" role="10QFUM">
                    <ref role="ehGHo" to="c8s6:4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
                  </node>
                </node>
                <node concept="2OqwBi" id="7vtgdGlUtcN" role="37vLTJ">
                  <node concept="2OqwBi" id="7vtgdGlUsHg" role="2Oq$k0">
                    <node concept="37vLTw" id="7vtgdGlUsmX" role="2Oq$k0">
                      <ref role="3cqZAo" node="7vtgdGlRjkD" resolve="newRule" />
                    </node>
                    <node concept="3TrEf2" id="7vtgdGlUsU6" role="2OqNvi">
                      <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cai" resolve="leftHandSide" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="7vtgdGlUtrr" role="2OqNvi">
                    <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2Gpval" id="7vtgdGlSYoA" role="3cqZAp">
              <node concept="2GrKxI" id="7vtgdGlSYoC" role="2Gsz3X">
                <property role="TrG5h" value="symb" />
              </node>
              <node concept="3clFbS" id="7vtgdGlSYoG" role="2LFqv$">
                <node concept="3clFbJ" id="7vtgdGlSZHT" role="3cqZAp">
                  <node concept="2ZW3vV" id="7vtgdGlT4pi" role="3clFbw">
                    <node concept="3uibUv" id="7vtgdGlT4Ua" role="2ZW6by">
                      <ref role="3uigEE" to="lc8y:~Terminal" resolve="Terminal" />
                    </node>
                    <node concept="2GrUjf" id="7vtgdGlSZIs" role="2ZW6bz">
                      <ref role="2Gs0qQ" node="7vtgdGlSYoC" resolve="symb" />
                    </node>
                  </node>
                  <node concept="3clFbS" id="7vtgdGlSZHV" role="3clFbx">
                    <node concept="3cpWs8" id="7vtgdGlUCr6" role="3cqZAp">
                      <node concept="3cpWsn" id="7vtgdGlUCr9" role="3cpWs9">
                        <property role="TrG5h" value="newSymb" />
                        <node concept="3Tqbb2" id="7vtgdGlUCr5" role="1tU5fm">
                          <ref role="ehGHo" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
                        </node>
                        <node concept="2OqwBi" id="7vtgdGlUCKy" role="33vP2m">
                          <node concept="35c_gC" id="7vtgdGlUCti" role="2Oq$k0">
                            <ref role="35c_gD" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
                          </node>
                          <node concept="LFhST" id="7vtgdGlUD8N" role="2OqNvi" />
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="7vtgdGlUDi1" role="3cqZAp">
                      <node concept="37vLTI" id="7vtgdGlUDYw" role="3clFbG">
                        <node concept="10QFUN" id="7vtgdGlUJ_T" role="37vLTx">
                          <node concept="2OqwBi" id="7vtgdGlUEFF" role="10QFUP">
                            <node concept="37vLTw" id="7vtgdGlUE1i" role="2Oq$k0">
                              <ref role="3cqZAo" node="7vtgdGlTBjo" resolve="decByName" />
                            </node>
                            <node concept="liA8E" id="7vtgdGlUFQt" role="2OqNvi">
                              <ref role="37wK5l" to="33ny:~HashMap.get(java.lang.Object)" resolve="get" />
                              <node concept="2OqwBi" id="3iSkm4L7xn_" role="37wK5m">
                                <node concept="2GrUjf" id="7vtgdGlUGoL" role="2Oq$k0">
                                  <ref role="2Gs0qQ" node="7vtgdGlSYoC" resolve="symb" />
                                </node>
                                <node concept="liA8E" id="3iSkm4L7ywJ" role="2OqNvi">
                                  <ref role="37wK5l" to=":^" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3Tqbb2" id="7vtgdGlUJ_U" role="10QFUM">
                            <ref role="ehGHo" to="c8s6:4vv0wgO7Ca8" resolve="TerminalDeclaration" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="7vtgdGlUDoo" role="37vLTJ">
                          <node concept="37vLTw" id="7vtgdGlUDhZ" role="2Oq$k0">
                            <ref role="3cqZAo" node="7vtgdGlUCr9" resolve="newSymb" />
                          </node>
                          <node concept="3TrEf2" id="7vtgdGlUD_e" role="2OqNvi">
                            <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cad" resolve="declaration" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="7vtgdGlULs1" role="3cqZAp">
                      <node concept="2OqwBi" id="7vtgdGlUO80" role="3clFbG">
                        <node concept="2OqwBi" id="7vtgdGlUM3R" role="2Oq$k0">
                          <node concept="37vLTw" id="7vtgdGlULrZ" role="2Oq$k0">
                            <ref role="3cqZAo" node="7vtgdGlRjkD" resolve="newRule" />
                          </node>
                          <node concept="3Tsc0h" id="7vtgdGlUMR3" role="2OqNvi">
                            <ref role="3TtcxE" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
                          </node>
                        </node>
                        <node concept="TSZUe" id="7vtgdGlURQB" role="2OqNvi">
                          <node concept="37vLTw" id="7vtgdGlUS2N" role="25WWJ7">
                            <ref role="3cqZAo" node="7vtgdGlUCr9" resolve="newSymb" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="9aQIb" id="7vtgdGlT5kc" role="9aQIa">
                    <node concept="3clFbS" id="7vtgdGlT5kd" role="9aQI4">
                      <node concept="3cpWs8" id="7vtgdGlUTdk" role="3cqZAp">
                        <node concept="3cpWsn" id="7vtgdGlUTdl" role="3cpWs9">
                          <property role="TrG5h" value="newSymb" />
                          <node concept="3Tqbb2" id="7vtgdGlUTdm" role="1tU5fm">
                            <ref role="ehGHo" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                          </node>
                          <node concept="2OqwBi" id="7vtgdGlUTdn" role="33vP2m">
                            <node concept="35c_gC" id="7vtgdGlUTdo" role="2Oq$k0">
                              <ref role="35c_gD" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                            </node>
                            <node concept="LFhST" id="7vtgdGlUTdp" role="2OqNvi" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="7vtgdGlUTdq" role="3cqZAp">
                        <node concept="37vLTI" id="7vtgdGlUTdr" role="3clFbG">
                          <node concept="10QFUN" id="7vtgdGlUTds" role="37vLTx">
                            <node concept="2OqwBi" id="7vtgdGlUTdt" role="10QFUP">
                              <node concept="37vLTw" id="7vtgdGlUTdu" role="2Oq$k0">
                                <ref role="3cqZAo" node="7vtgdGlTBjo" resolve="decByName" />
                              </node>
                              <node concept="liA8E" id="7vtgdGlUTdv" role="2OqNvi">
                                <ref role="37wK5l" to="33ny:~HashMap.get(java.lang.Object)" resolve="get" />
                                <node concept="2OqwBi" id="3iSkm4L7_ud" role="37wK5m">
                                  <node concept="2GrUjf" id="7vtgdGlUTdx" role="2Oq$k0">
                                    <ref role="2Gs0qQ" node="7vtgdGlSYoC" resolve="symb" />
                                  </node>
                                  <node concept="liA8E" id="3iSkm4L7ABz" role="2OqNvi">
                                    <ref role="37wK5l" to=":^" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3Tqbb2" id="7vtgdGlUTdz" role="10QFUM">
                              <ref role="ehGHo" to="c8s6:4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="7vtgdGlUVC5" role="37vLTJ">
                            <node concept="37vLTw" id="7vtgdGlUTd_" role="2Oq$k0">
                              <ref role="3cqZAo" node="7vtgdGlUTdl" resolve="newSymb" />
                            </node>
                            <node concept="3TrEf2" id="7vtgdGlUVYQ" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbF" id="7vtgdGlUTdB" role="3cqZAp">
                        <node concept="2OqwBi" id="7vtgdGlUTdC" role="3clFbG">
                          <node concept="2OqwBi" id="7vtgdGlUTdD" role="2Oq$k0">
                            <node concept="37vLTw" id="7vtgdGlUTdE" role="2Oq$k0">
                              <ref role="3cqZAo" node="7vtgdGlRjkD" resolve="newRule" />
                            </node>
                            <node concept="3Tsc0h" id="7vtgdGlUTdF" role="2OqNvi">
                              <ref role="3TtcxE" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
                            </node>
                          </node>
                          <node concept="TSZUe" id="7vtgdGlUTdG" role="2OqNvi">
                            <node concept="37vLTw" id="7vtgdGlUTdH" role="25WWJ7">
                              <ref role="3cqZAo" node="7vtgdGlUTdl" resolve="newSymb" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3iSkm4L7tZl" role="2GsD0m">
                <node concept="2GrUjf" id="7vtgdGlSYPQ" role="2Oq$k0">
                  <ref role="2Gs0qQ" node="7vtgdGlPEVg" resolve="rule" />
                </node>
                <node concept="liA8E" id="3iSkm4L7uRD" role="2OqNvi">
                  <ref role="37wK5l" to=":^" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7vtgdGlRkb_" role="3cqZAp">
              <node concept="2OqwBi" id="7vtgdGlRm5i" role="3clFbG">
                <node concept="2OqwBi" id="7vtgdGlRkhW" role="2Oq$k0">
                  <node concept="37vLTw" id="7vtgdGlRkbz" role="2Oq$k0">
                    <ref role="3cqZAo" node="7vtgdGlR1gb" resolve="table" />
                  </node>
                  <node concept="3Tsc0h" id="7vtgdGlRkuM" role="2OqNvi">
                    <ref role="3TtcxE" to="c8s6:3v$U6ONNksI" resolve="rules" />
                  </node>
                </node>
                <node concept="TSZUe" id="7vtgdGlRqkj" role="2OqNvi">
                  <node concept="37vLTw" id="7vtgdGlRqye" role="25WWJ7">
                    <ref role="3cqZAo" node="7vtgdGlRjkD" resolve="newRule" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="3iSkm4L7hxq" role="2GsD0m">
            <node concept="37vLTw" id="7vtgdGlVZce" role="2Oq$k0">
              <ref role="3cqZAo" node="7vtgdGlVSqo" resolve="g" />
            </node>
            <node concept="liA8E" id="3iSkm4L7inw" role="2OqNvi">
              <ref role="37wK5l" to=":^" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7vtgdGlQuoD" role="3cqZAp">
          <node concept="2OqwBi" id="7vtgdGlQyM4" role="3clFbG">
            <node concept="2OqwBi" id="7vtgdGlQwUM" role="2Oq$k0">
              <node concept="2OqwBi" id="7vtgdGlQuoz" role="2Oq$k0">
                <node concept="2WthIp" id="7vtgdGlQuoA" role="2Oq$k0" />
                <node concept="3gHZIF" id="7vtgdGlQuoC" role="2OqNvi">
                  <ref role="2WH_rO" node="6cQ5Sd6oWpT" resolve="node" />
                </node>
              </node>
              <node concept="3Tsc0h" id="7vtgdGlQxuO" role="2OqNvi">
                <ref role="3TtcxE" to="c8s6:4vv0wgOalpj" resolve="derivedInfo" />
              </node>
            </node>
            <node concept="TSZUe" id="7vtgdGlQAQx" role="2OqNvi">
              <node concept="37vLTw" id="7vtgdGlRiWu" role="25WWJ7">
                <ref role="3cqZAo" node="7vtgdGlR1gb" resolve="table" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2S4$dB" id="6cQ5Sd6oWpT" role="1NuT2Z">
      <property role="TrG5h" value="node" />
      <node concept="3Tm6S6" id="6cQ5Sd6oWpU" role="1B3o_S" />
      <node concept="1oajcY" id="6cQ5Sd6oWpV" role="1oa70y" />
      <node concept="3Tqbb2" id="6cQ5Sd6oWlt" role="1tU5fm">
        <ref role="ehGHo" to="c8s6:4vv0wgO7Ca6" resolve="Grammar" />
      </node>
    </node>
  </node>
  <node concept="tC5Ba" id="6cQ5Sd6nOj9">
    <property role="TrG5h" value="adaptGrammar" />
    <node concept="ftmFs" id="6cQ5Sd6nOja" role="ftER_">
      <node concept="tCFHf" id="6cQ5Sd6nUj0" role="ftvYc">
        <ref role="tCJdB" node="6cQ5Sd6nOiN" resolve="adaptGrammar" />
      </node>
    </node>
    <node concept="tT9cl" id="6cQ5Sd6nUj2" role="2f5YQi">
      <ref role="tU$_T" to="tprs:hyf4LYI" resolve="Tools" />
    </node>
  </node>
</model>

