<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:08f8613a-9ff2-487d-bf55-5ceca2c577dd(jetbrains.mps.samples.grammarLanguage.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpc2" ref="r:00000000-0000-4000-0000-011c8959029e(jetbrains.mps.lang.editor.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="6054523464627964745" name="jetbrains.mps.lang.structure.structure.AttributeInfo_AttributedConcept" flags="ng" index="trNpa">
        <reference id="6054523464627965081" name="concept" index="trN6q" />
      </concept>
      <concept id="2992811758677295509" name="jetbrains.mps.lang.structure.structure.AttributeInfo" flags="ng" index="M6xJ_">
        <property id="7588428831955550663" name="role" index="Hh88m" />
        <child id="7588428831947959310" name="attributed" index="EQaZv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="4vv0wgO7Ca6">
    <property role="EcuMT" value="5178860313576899206" />
    <property role="TrG5h" value="Grammar" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4vv0wgO7Cbt" role="1TKVEi">
      <property role="IQ2ns" value="5178860313576899293" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="terminals" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgO7Ca8" resolve="TerminalDeclaration" />
    </node>
    <node concept="1TJgyj" id="4vv0wgO7Cbv" role="1TKVEi">
      <property role="IQ2ns" value="5178860313576899295" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="nonterminals" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
    </node>
    <node concept="1TJgyj" id="4vv0wgO7CcX" role="1TKVEi">
      <property role="IQ2ns" value="5178860313576899389" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="rules" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgO7Ca7" resolve="GrammarRule" />
    </node>
    <node concept="1TJgyj" id="4vv0wgOalpj" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577608787" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="derivedInfo" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgOalpi" resolve="DerivedInfo" />
    </node>
    <node concept="PrWs8" id="4vv0wgO7CaW" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="4vv0wgOg_1i" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO7Ca7">
    <property role="EcuMT" value="5178860313576899207" />
    <property role="TrG5h" value="GrammarRule" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4vv0wgO7Cai" role="1TKVEi">
      <property role="IQ2ns" value="5178860313576899218" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="leftHandSide" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Caf" resolve="NonterminalReference" />
    </node>
    <node concept="1TJgyj" id="4vv0wgO7Cak" role="1TKVEi">
      <property role="IQ2ns" value="5178860313576899220" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="rightHandSide" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="4vv0wgO7Cah" resolve="SymbolReference" />
    </node>
    <node concept="PrWs8" id="4vv0wgO8DoP" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO7Ca8">
    <property role="EcuMT" value="5178860313576899208" />
    <property role="TrG5h" value="TerminalDeclaration" />
    <ref role="1TJDcQ" node="4vv0wgO7VEI" resolve="SymbolDeclaration" />
    <node concept="PrWs8" id="4vv0wgO7LYx" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO7Ca9">
    <property role="EcuMT" value="5178860313576899209" />
    <property role="TrG5h" value="NonterminalDeclaration" />
    <ref role="1TJDcQ" node="4vv0wgO7VEI" resolve="SymbolDeclaration" />
    <node concept="PrWs8" id="4vv0wgO7LYz" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO7Cac">
    <property role="EcuMT" value="5178860313576899212" />
    <property role="TrG5h" value="TerminalReference" />
    <ref role="1TJDcQ" node="4vv0wgO7Cah" resolve="SymbolReference" />
    <node concept="1TJgyj" id="4vv0wgO7Cad" role="1TKVEi">
      <property role="IQ2ns" value="5178860313576899213" />
      <property role="20kJfa" value="declaration" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Ca8" resolve="TerminalDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO7Caf">
    <property role="EcuMT" value="5178860313576899215" />
    <property role="TrG5h" value="NonterminalReference" />
    <ref role="1TJDcQ" node="4vv0wgO7Cah" resolve="SymbolReference" />
    <node concept="1TJgyj" id="4vv0wgO7Cag" role="1TKVEi">
      <property role="IQ2ns" value="5178860313576899216" />
      <property role="20kJfa" value="declaration" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Ca9" resolve="NonterminalDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO7Cah">
    <property role="EcuMT" value="5178860313576899217" />
    <property role="TrG5h" value="SymbolReference" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="4vv0wgO7VEI">
    <property role="EcuMT" value="5178860313576979118" />
    <property role="TrG5h" value="SymbolDeclaration" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="4vv0wgO7VEJ" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO8jGv">
    <property role="EcuMT" value="5178860313577077535" />
    <property role="TrG5h" value="GrammarExamples" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="sample" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4vv0wgO8klz" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577080163" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <property role="20kJfa" value="grammar" />
      <ref role="20lvS9" node="4vv0wgO7Ca6" resolve="Grammar" />
    </node>
    <node concept="1TJgyj" id="4vv0wgO8kku" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577080094" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="lines" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgO8jGw" resolve="ExampleLine" />
    </node>
    <node concept="PrWs8" id="4vv0wgO8kmK" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="3v$U6ONMJy5" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO8jGw">
    <property role="EcuMT" value="5178860313577077536" />
    <property role="TrG5h" value="ExampleLine" />
    <property role="3GE5qa" value="sample" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="4vv0wgO9JCv" role="1TKVEl">
      <property role="IQ2nx" value="5178860313577454111" />
      <property role="TrG5h" value="expand" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyj" id="4vv0wgO8jGx" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577077537" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="leftHandSide" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Caf" resolve="NonterminalReference" />
    </node>
    <node concept="1TJgyj" id="4vv0wgO8jGz" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577077539" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="rightHandSide" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO8jGL" resolve="SymbolInstance" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO8jGA">
    <property role="EcuMT" value="5178860313577077542" />
    <property role="3GE5qa" value="sample" />
    <property role="TrG5h" value="TerminalInstance" />
    <ref role="1TJDcQ" node="4vv0wgO8jGL" resolve="SymbolInstance" />
    <node concept="1TJgyj" id="4vv0wgO8jGB" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577077543" />
      <property role="20kJfa" value="symbol" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Ca8" resolve="TerminalDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO8jGD">
    <property role="EcuMT" value="5178860313577077545" />
    <property role="3GE5qa" value="sample" />
    <property role="TrG5h" value="NonterminalInstance" />
    <ref role="1TJDcQ" node="4vv0wgO8jGL" resolve="SymbolInstance" />
    <node concept="1TJgyj" id="4vv0wgO8jGN" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577077555" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="children" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgO8jGL" resolve="SymbolInstance" />
    </node>
    <node concept="1TJgyj" id="4vv0wgO8jGE" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577077546" />
      <property role="20kJfa" value="rule" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Ca7" resolve="GrammarRule" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgO8jGL">
    <property role="EcuMT" value="5178860313577077553" />
    <property role="3GE5qa" value="sample" />
    <property role="TrG5h" value="SymbolInstance" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="4vv0wgOacKR">
    <property role="EcuMT" value="5178860313577573431" />
    <property role="TrG5h" value="LL1Table" />
    <property role="3GE5qa" value="LL1" />
    <ref role="1TJDcQ" node="4vv0wgOalpi" resolve="DerivedInfo" />
    <node concept="1TJgyj" id="4vv0wgOacL8" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577573448" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <property role="20kJfa" value="rows" />
      <ref role="20lvS9" node="4vv0wgOacKS" resolve="LL1Row" />
    </node>
    <node concept="PrWs8" id="7DEtAv3Szm7" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgOacKS">
    <property role="EcuMT" value="5178860313577573432" />
    <property role="TrG5h" value="LL1Row" />
    <property role="3GE5qa" value="LL1" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4vv0wgOacKT" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577573433" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="symbol" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Caf" resolve="NonterminalReference" />
    </node>
    <node concept="1TJgyj" id="4vv0wgOacKV" role="1TKVEi">
      <property role="IQ2ns" value="5178860313577573435" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="firstSymbol" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgOnpJi" resolve="SymbolWithRules" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgOalpi">
    <property role="EcuMT" value="5178860313577608786" />
    <property role="TrG5h" value="DerivedInfo" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="4vv0wgOgoDJ">
    <property role="EcuMT" value="5178860313579194991" />
    <property role="TrG5h" value="LR0Table" />
    <property role="3GE5qa" value="LR0" />
    <ref role="1TJDcQ" node="4vv0wgOalpi" resolve="DerivedInfo" />
    <node concept="1TJgyj" id="4vv0wgOgoDL" role="1TKVEi">
      <property role="IQ2ns" value="5178860313579194993" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="items" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgO7Ca7" resolve="GrammarRule" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgOgoDN">
    <property role="EcuMT" value="5178860313579194995" />
    <property role="3GE5qa" value="LR0" />
    <property role="TrG5h" value="RuleCursor" />
    <ref role="1TJDcQ" to="tpck:BpxLfMhSxq" resolve="ChildAttribute" />
    <node concept="M6xJ_" id="4vv0wgOgoDO" role="lGtFl">
      <property role="Hh88m" value="cursor" />
      <node concept="trNpa" id="4vv0wgOgoDQ" role="EQaZv">
        <ref role="trN6q" node="4vv0wgO7Ca7" resolve="GrammarRule" />
      </node>
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgOidGS">
    <property role="EcuMT" value="5178860313579674424" />
    <property role="TrG5h" value="LL1Automata" />
    <property role="3GE5qa" value="LL1" />
    <ref role="1TJDcQ" node="4vv0wgOalpi" resolve="DerivedInfo" />
  </node>
  <node concept="1TIwiD" id="4vv0wgOnpJc">
    <property role="EcuMT" value="5178860313581034444" />
    <property role="3GE5qa" value="LL1" />
    <property role="TrG5h" value="RulePath" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4vv0wgOnpJg" role="1TKVEi">
      <property role="IQ2ns" value="5178860313581034448" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <property role="20kJfa" value="rules" />
      <ref role="20lvS9" node="4vv0wgOnpJd" resolve="RuleReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgOnpJd">
    <property role="EcuMT" value="5178860313581034445" />
    <property role="3GE5qa" value="LL1" />
    <property role="TrG5h" value="RuleReference" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4vv0wgOnpJe" role="1TKVEi">
      <property role="IQ2ns" value="5178860313581034446" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <property role="20kJfa" value="declaration" />
      <ref role="20lvS9" node="4vv0wgO7Ca7" resolve="GrammarRule" />
    </node>
  </node>
  <node concept="1TIwiD" id="4vv0wgOnpJi">
    <property role="EcuMT" value="5178860313581034450" />
    <property role="3GE5qa" value="LL1" />
    <property role="TrG5h" value="SymbolWithRules" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7DEtAv3PZGf" role="1TKVEi">
      <property role="IQ2ns" value="8820993008197958415" />
      <property role="20kJfa" value="symbol" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7VEI" resolve="SymbolDeclaration" />
    </node>
    <node concept="1TJgyj" id="4vv0wgOnpJj" role="1TKVEi">
      <property role="IQ2ns" value="5178860313581034451" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <property role="20kJfa" value="paths" />
      <ref role="20lvS9" node="4vv0wgOnpJc" resolve="RulePath" />
    </node>
  </node>
  <node concept="1TIwiD" id="3v$U6ONMnP1">
    <property role="EcuMT" value="4027599522493988161" />
    <property role="TrG5h" value="Transform" />
    <property role="3GE5qa" value="transform" />
    <property role="34LRSv" value="transform" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7DEtAv3yrWD" role="1TKVEi">
      <property role="IQ2ns" value="8820993008192831273" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="symbol" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Caf" resolve="NonterminalReference" />
    </node>
    <node concept="1TJgyj" id="7DEtAv3oLdb" role="1TKVEi">
      <property role="IQ2ns" value="8820993008190296907" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="fromPattern" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="7DEtAv3oLdE" resolve="AbstractTransformPattern" />
    </node>
    <node concept="1TJgyi" id="7DEtAv3lMVa" role="1TKVEl">
      <property role="IQ2nx" value="8820993008189517514" />
      <property role="TrG5h" value="side" />
      <ref role="AX2Wp" to="tpc2:3Ftr4R6BFev" resolve="TransformationLocation_SideTransform_PlaceInCell" />
    </node>
    <node concept="1TJgyj" id="7DEtAv3ysTw" role="1TKVEi">
      <property role="IQ2ns" value="8820993008192835168" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="toPattern" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="7DEtAv3oLdE" resolve="AbstractTransformPattern" />
    </node>
  </node>
  <node concept="1TIwiD" id="3v$U6ONMnQB">
    <property role="EcuMT" value="4027599522493988263" />
    <property role="3GE5qa" value="transform" />
    <property role="TrG5h" value="TransformationsTable" />
    <property role="34LRSv" value="transforms" />
    <ref role="1TJDcQ" node="4vv0wgOalpi" resolve="DerivedInfo" />
    <node concept="1TJgyj" id="3v$U6ONMnQE" role="1TKVEi">
      <property role="IQ2ns" value="4027599522493988266" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="transformations" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3v$U6ONMnP1" resolve="Transform" />
    </node>
    <node concept="PrWs8" id="7DEtAv408Sm" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="3v$U6ONNQeH" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="3v$U6ONNksH">
    <property role="EcuMT" value="4027599522494236461" />
    <property role="3GE5qa" value="transform" />
    <property role="TrG5h" value="AdditionalRulesTable" />
    <property role="34LRSv" value="additional rules" />
    <ref role="1TJDcQ" node="4vv0wgOalpi" resolve="DerivedInfo" />
    <node concept="1TJgyj" id="3v$U6ONNksI" role="1TKVEi">
      <property role="IQ2ns" value="4027599522494236462" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="rules" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="4vv0wgO7Ca7" resolve="GrammarRule" />
    </node>
  </node>
  <node concept="1TIwiD" id="7DEtAv3oLdr">
    <property role="EcuMT" value="8820993008190296923" />
    <property role="3GE5qa" value="transform" />
    <property role="TrG5h" value="TransformPatternRule" />
    <ref role="1TJDcQ" node="7DEtAv3oLdE" resolve="AbstractTransformPattern" />
    <node concept="1TJgyj" id="7DEtAv3oLd$" role="1TKVEi">
      <property role="IQ2ns" value="8820993008190296932" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="childPatterns" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="7DEtAv3oLdE" resolve="AbstractTransformPattern" />
    </node>
    <node concept="1TJgyj" id="7DEtAv3oLdy" role="1TKVEi">
      <property role="IQ2ns" value="8820993008190296930" />
      <property role="20kJfa" value="rule" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="4vv0wgO7Ca7" resolve="GrammarRule" />
    </node>
  </node>
  <node concept="1TIwiD" id="7DEtAv3oLdB">
    <property role="EcuMT" value="8820993008190296935" />
    <property role="3GE5qa" value="transform" />
    <property role="TrG5h" value="TransformPatternAny" />
    <property role="34LRSv" value="_" />
    <ref role="1TJDcQ" node="7DEtAv3oLdE" resolve="AbstractTransformPattern" />
  </node>
  <node concept="1TIwiD" id="7DEtAv3oLdE">
    <property role="EcuMT" value="8820993008190296938" />
    <property role="3GE5qa" value="transform" />
    <property role="TrG5h" value="AbstractTransformPattern" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
</model>

