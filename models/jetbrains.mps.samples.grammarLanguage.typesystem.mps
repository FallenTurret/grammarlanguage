<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b114ebe3-7bf9-466a-a3bb-5d6f4cf158b9(jetbrains.mps.samples.grammarLanguage.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="4" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="c8s6" ref="r:08f8613a-9ff2-487d-bf55-5ceca2c577dd(jetbrains.mps.samples.grammarLanguage.structure)" />
    <import index="h9ya" ref="r:194bf60c-7dbf-4609-8b8d-8f296167a58e(jetbrains.mps.samples.grammarLanguage.behavior)" />
    <import index="tpc2" ref="r:00000000-0000-4000-0000-011c8959029e(jetbrains.mps.lang.editor.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271283259" name="jetbrains.mps.baseLanguage.structure.NPEEqualsExpression" flags="nn" index="17R0WA" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1175517400280" name="jetbrains.mps.lang.typesystem.structure.AssertStatement" flags="nn" index="2Mj0R9">
        <child id="1175517761460" name="condition" index="2MkoU_" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="4705942098322609812" name="jetbrains.mps.lang.smodel.structure.EnumMember_IsOperation" flags="ng" index="21noJN">
        <child id="4705942098322609813" name="member" index="21noJM" />
      </concept>
      <concept id="4705942098322467729" name="jetbrains.mps.lang.smodel.structure.EnumMemberReference" flags="ng" index="21nZrQ">
        <reference id="4705942098322467736" name="decl" index="21nZrZ" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1154546950173" name="jetbrains.mps.lang.smodel.structure.ConceptReference" flags="ng" index="3gn64h">
        <reference id="1154546997487" name="concept" index="3gnhBz" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="6039268229364358244" name="jetbrains.mps.lang.smodel.structure.ExactConceptCase" flags="ng" index="1pnPoh">
        <child id="6039268229364358388" name="body" index="1pnPq1" />
        <child id="6039268229364358387" name="concept" index="1pnPq6" />
      </concept>
      <concept id="5944356402132808749" name="jetbrains.mps.lang.smodel.structure.ConceptSwitchStatement" flags="nn" index="1_3QMa">
        <child id="5944356402132808753" name="case" index="1_3QMm" />
        <child id="5944356402132808752" name="expression" index="1_3QMn" />
      </concept>
      <concept id="5944356402132808754" name="jetbrains.mps.lang.smodel.structure.SubconceptCase" flags="ng" index="1_3QMl">
        <child id="1163670677455" name="concept" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1172650591544" name="jetbrains.mps.baseLanguage.collections.structure.SkipOperation" flags="nn" index="7r0gD">
        <child id="1172658456740" name="elementsToSkip" index="7T0AP" />
      </concept>
      <concept id="1172664342967" name="jetbrains.mps.baseLanguage.collections.structure.TakeOperation" flags="nn" index="8ftyA">
        <child id="1172664372046" name="elementsToTake" index="8f$Dv" />
      </concept>
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1151702311717" name="jetbrains.mps.baseLanguage.collections.structure.ToListOperation" flags="nn" index="ANE8D" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="9042586985346099698" name="jetbrains.mps.baseLanguage.collections.structure.MultiForEachStatement" flags="nn" index="1_o_46">
        <child id="9042586985346099734" name="forEach" index="1_o_by" />
      </concept>
      <concept id="9042586985346099733" name="jetbrains.mps.baseLanguage.collections.structure.MultiForEachPair" flags="ng" index="1_o_bx">
        <child id="9042586985346099778" name="variable" index="1_o_aQ" />
        <child id="9042586985346099735" name="input" index="1_o_bz" />
      </concept>
      <concept id="9042586985346099736" name="jetbrains.mps.baseLanguage.collections.structure.MultiForEachVariable" flags="ng" index="1_o_bG" />
      <concept id="8293956702609956630" name="jetbrains.mps.baseLanguage.collections.structure.MultiForEachVariableReference" flags="nn" index="3M$PaV">
        <reference id="8293956702609966325" name="variable" index="3M$S_o" />
      </concept>
    </language>
  </registry>
  <node concept="18kY7G" id="4vv0wgO9a0b">
    <property role="TrG5h" value="check_NonterminalInstance" />
    <property role="3GE5qa" value="sample" />
    <node concept="3clFbS" id="4vv0wgO9a0c" role="18ibNy">
      <node concept="3clFbJ" id="4vv0wgO9aAX" role="3cqZAp">
        <node concept="3clFbS" id="4vv0wgO9aAZ" role="3clFbx">
          <node concept="1_o_46" id="4vv0wgO9kK3" role="3cqZAp">
            <node concept="1_o_bx" id="4vv0wgO9kK5" role="1_o_by">
              <node concept="1_o_bG" id="4vv0wgO9kK7" role="1_o_aQ">
                <property role="TrG5h" value="formal" />
              </node>
              <node concept="2OqwBi" id="4vv0wgO9kKA" role="1_o_bz">
                <node concept="2OqwBi" id="4vv0wgO9kKB" role="2Oq$k0">
                  <node concept="1YBJjd" id="4vv0wgO9kKC" role="2Oq$k0">
                    <ref role="1YBMHb" node="4vv0wgO9a0e" resolve="instance" />
                  </node>
                  <node concept="3TrEf2" id="4vv0wgO9kKD" role="2OqNvi">
                    <ref role="3Tt5mk" to="c8s6:4vv0wgO8jGE" resolve="rule" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="4vv0wgO9kKE" role="2OqNvi">
                  <ref role="3TtcxE" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
                </node>
              </node>
            </node>
            <node concept="1_o_bx" id="4vv0wgO9kPz" role="1_o_by">
              <node concept="1_o_bG" id="4vv0wgO9kP$" role="1_o_aQ">
                <property role="TrG5h" value="actual" />
              </node>
              <node concept="2OqwBi" id="4vv0wgO9lm9" role="1_o_bz">
                <node concept="1YBJjd" id="4vv0wgO9lma" role="2Oq$k0">
                  <ref role="1YBMHb" node="4vv0wgO9a0e" resolve="instance" />
                </node>
                <node concept="3Tsc0h" id="4vv0wgO9lmb" role="2OqNvi">
                  <ref role="3TtcxE" to="c8s6:4vv0wgO8jGN" resolve="children" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="4vv0wgO9kKb" role="2LFqv$">
              <node concept="1_3QMa" id="4vv0wgO9lXz" role="3cqZAp">
                <node concept="1pnPoh" id="4vv0wgO9lZt" role="1_3QMm">
                  <node concept="3gn64h" id="4vv0wgO9lZA" role="1pnPq6">
                    <ref role="3gnhBz" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                  </node>
                  <node concept="3clFbS" id="4vv0wgO9lZx" role="1pnPq1">
                    <node concept="2Mj0R9" id="4vv0wgO9lZD" role="3cqZAp">
                      <node concept="1Wc70l" id="4vv0wgO9mDy" role="2MkoU_">
                        <node concept="3clFbC" id="4vv0wgO9ooV" role="3uHU7w">
                          <node concept="2OqwBi" id="4vv0wgO9p2L" role="3uHU7w">
                            <node concept="1PxgMI" id="4vv0wgO9oN$" role="2Oq$k0">
                              <node concept="chp4Y" id="4vv0wgO9oPU" role="3oSUPX">
                                <ref role="cht4Q" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                              </node>
                              <node concept="3M$PaV" id="4vv0wgO9ouA" role="1m5AlR">
                                <ref role="3M$S_o" node="4vv0wgO9kK7" resolve="formal" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="4vv0wgO9pcB" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="4vv0wgO9o0V" role="3uHU7B">
                            <node concept="2OqwBi" id="4vv0wgO9nDl" role="2Oq$k0">
                              <node concept="2OqwBi" id="4vv0wgO9nia" role="2Oq$k0">
                                <node concept="1PxgMI" id="4vv0wgO9n3D" role="2Oq$k0">
                                  <node concept="chp4Y" id="4vv0wgO9n8k" role="3oSUPX">
                                    <ref role="cht4Q" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
                                  </node>
                                  <node concept="3M$PaV" id="4vv0wgO9mEE" role="1m5AlR">
                                    <ref role="3M$S_o" node="4vv0wgO9kP$" resolve="actual" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="4vv0wgO9nty" role="2OqNvi">
                                  <ref role="3Tt5mk" to="c8s6:4vv0wgO8jGE" resolve="rule" />
                                </node>
                              </node>
                              <node concept="3TrEf2" id="4vv0wgO9nOo" role="2OqNvi">
                                <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cai" resolve="leftHandSide" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="4vv0wgO9oaW" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="4vv0wgO9m8W" role="3uHU7B">
                          <node concept="3M$PaV" id="4vv0wgO9lZX" role="2Oq$k0">
                            <ref role="3M$S_o" node="4vv0wgO9kP$" resolve="actual" />
                          </node>
                          <node concept="1mIQ4w" id="4vv0wgO9mi_" role="2OqNvi">
                            <node concept="chp4Y" id="4vv0wgO9mkJ" role="cj9EA">
                              <ref role="cht4Q" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Xl_RD" id="4vv0wgO9peI" role="2MkJ7o">
                        <property role="Xl_RC" value="illegal argument" />
                      </node>
                      <node concept="3M$PaV" id="4vv0wgO9phm" role="2OEOjV">
                        <ref role="3M$S_o" node="4vv0wgO9kP$" resolve="actual" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="1pnPoh" id="4vv0wgO9pjZ" role="1_3QMm">
                  <node concept="3gn64h" id="4vv0wgO9pky" role="1pnPq6">
                    <ref role="3gnhBz" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
                  </node>
                  <node concept="3clFbS" id="4vv0wgO9pk3" role="1pnPq1">
                    <node concept="2Mj0R9" id="4vv0wgO9pk_" role="3cqZAp">
                      <node concept="1Wc70l" id="4vv0wgO9pkA" role="2MkoU_">
                        <node concept="3clFbC" id="4vv0wgO9pkB" role="3uHU7w">
                          <node concept="2OqwBi" id="4vv0wgO9pkC" role="3uHU7w">
                            <node concept="1PxgMI" id="4vv0wgO9pkD" role="2Oq$k0">
                              <node concept="chp4Y" id="4vv0wgO9qoJ" role="3oSUPX">
                                <ref role="cht4Q" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
                              </node>
                              <node concept="3M$PaV" id="4vv0wgO9pkF" role="1m5AlR">
                                <ref role="3M$S_o" node="4vv0wgO9kK7" resolve="formal" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="4vv0wgO9q_Y" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cad" resolve="declaration" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="4vv0wgO9pkJ" role="3uHU7B">
                            <node concept="1PxgMI" id="4vv0wgO9pkK" role="2Oq$k0">
                              <node concept="chp4Y" id="4vv0wgO9pPE" role="3oSUPX">
                                <ref role="cht4Q" to="c8s6:4vv0wgO8jGA" resolve="TerminalInstance" />
                              </node>
                              <node concept="3M$PaV" id="4vv0wgO9pkM" role="1m5AlR">
                                <ref role="3M$S_o" node="4vv0wgO9kP$" resolve="actual" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="4vv0wgO9q4p" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO8jGB" resolve="symbol" />
                            </node>
                          </node>
                        </node>
                        <node concept="2OqwBi" id="4vv0wgO9pkQ" role="3uHU7B">
                          <node concept="3M$PaV" id="4vv0wgO9pkR" role="2Oq$k0">
                            <ref role="3M$S_o" node="4vv0wgO9kP$" resolve="actual" />
                          </node>
                          <node concept="1mIQ4w" id="4vv0wgO9pkS" role="2OqNvi">
                            <node concept="chp4Y" id="4vv0wgO9pJ_" role="cj9EA">
                              <ref role="cht4Q" to="c8s6:4vv0wgO8jGA" resolve="TerminalInstance" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="Xl_RD" id="4vv0wgO9pkU" role="2MkJ7o">
                        <property role="Xl_RC" value="illegal argument" />
                      </node>
                      <node concept="3M$PaV" id="4vv0wgO9pkV" role="2OEOjV">
                        <ref role="3M$S_o" node="4vv0wgO9kP$" resolve="actual" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="4vv0wgO9lXN" role="1_3QMn">
                  <node concept="3M$PaV" id="4vv0wgO9lXB" role="2Oq$k0">
                    <ref role="3M$S_o" node="4vv0wgO9kK7" resolve="formal" />
                  </node>
                  <node concept="2yIwOk" id="4vv0wgO9lZe" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbC" id="4vv0wgO9fDI" role="3clFbw">
          <node concept="2OqwBi" id="4vv0wgO9cGg" role="3uHU7B">
            <node concept="2OqwBi" id="4vv0wgO9aqF" role="2Oq$k0">
              <node concept="2OqwBi" id="4vv0wgO9a8g" role="2Oq$k0">
                <node concept="1YBJjd" id="4vv0wgO9a0n" role="2Oq$k0">
                  <ref role="1YBMHb" node="4vv0wgO9a0e" resolve="instance" />
                </node>
                <node concept="3TrEf2" id="4vv0wgO9agv" role="2OqNvi">
                  <ref role="3Tt5mk" to="c8s6:4vv0wgO8jGE" resolve="rule" />
                </node>
              </node>
              <node concept="3Tsc0h" id="4vv0wgO9a$E" role="2OqNvi">
                <ref role="3TtcxE" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
              </node>
            </node>
            <node concept="34oBXx" id="4vv0wgO9dU1" role="2OqNvi" />
          </node>
          <node concept="2OqwBi" id="4vv0wgO9iRf" role="3uHU7w">
            <node concept="2OqwBi" id="4vv0wgO9g5z" role="2Oq$k0">
              <node concept="1YBJjd" id="4vv0wgO9g5$" role="2Oq$k0">
                <ref role="1YBMHb" node="4vv0wgO9a0e" resolve="instance" />
              </node>
              <node concept="3Tsc0h" id="4vv0wgO9h3x" role="2OqNvi">
                <ref role="3TtcxE" to="c8s6:4vv0wgO8jGN" resolve="children" />
              </node>
            </node>
            <node concept="34oBXx" id="4vv0wgO9kDI" role="2OqNvi" />
          </node>
        </node>
        <node concept="9aQIb" id="4vv0wgO9kFB" role="9aQIa">
          <node concept="3clFbS" id="4vv0wgO9kFC" role="9aQI4">
            <node concept="2MkqsV" id="4vv0wgO9kHL" role="3cqZAp">
              <node concept="Xl_RD" id="4vv0wgO9kHX" role="2MkJ7o">
                <property role="Xl_RC" value="argument number doesn't match" />
              </node>
              <node concept="1YBJjd" id="4vv0wgO9kIt" role="2OEOjV">
                <ref role="1YBMHb" node="4vv0wgO9a0e" resolve="instance" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4vv0wgO9a0e" role="1YuTPh">
      <property role="TrG5h" value="instance" />
      <ref role="1YaFvo" to="c8s6:4vv0wgO8jGD" resolve="NonterminalInstance" />
    </node>
  </node>
  <node concept="18kY7G" id="7DEtAv3lfz5">
    <property role="TrG5h" value="check_Transform" />
    <property role="3GE5qa" value="transform" />
    <node concept="3clFbS" id="7DEtAv3lfz6" role="18ibNy">
      <node concept="1_3QMa" id="7DEtAv3qm_o" role="3cqZAp">
        <node concept="2OqwBi" id="7DEtAv3qnGu" role="1_3QMn">
          <node concept="2OqwBi" id="7DEtAv3qmIY" role="2Oq$k0">
            <node concept="1YBJjd" id="7DEtAv3qmBM" role="2Oq$k0">
              <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
            </node>
            <node concept="3TrEf2" id="7DEtAv3qn$S" role="2OqNvi">
              <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
            </node>
          </node>
          <node concept="2yIwOk" id="7DEtAv3qocP" role="2OqNvi" />
        </node>
        <node concept="1_3QMl" id="7DEtAv3qode" role="1_3QMm">
          <node concept="3gn64h" id="7DEtAv3qodk" role="3Kbmr1">
            <ref role="3gnhBz" to="c8s6:7DEtAv3oLdr" resolve="TransformPatternRule" />
          </node>
          <node concept="3clFbS" id="7DEtAv3qodg" role="3Kbo56">
            <node concept="2Mj0R9" id="7DEtAv3qjIP" role="3cqZAp">
              <node concept="Xl_RD" id="7DEtAv3qjIQ" role="2MkJ7o">
                <property role="Xl_RC" value="Transformation should be defined for the same nonterminal" />
              </node>
              <node concept="1YBJjd" id="7DEtAv3qjIR" role="2OEOjV">
                <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
              </node>
              <node concept="3clFbC" id="7DEtAv3qjIS" role="2MkoU_">
                <node concept="2OqwBi" id="7DEtAv3qjIT" role="3uHU7B">
                  <node concept="2OqwBi" id="7DEtAv3qjIU" role="2Oq$k0">
                    <node concept="2OqwBi" id="7DEtAv3qli2" role="2Oq$k0">
                      <node concept="1PxgMI" id="7DEtAv3qkMY" role="2Oq$k0">
                        <node concept="chp4Y" id="7DEtAv3ql7D" role="3oSUPX">
                          <ref role="cht4Q" to="c8s6:7DEtAv3oLdr" resolve="TransformPatternRule" />
                        </node>
                        <node concept="2OqwBi" id="7DEtAv3qjIW" role="1m5AlR">
                          <node concept="1YBJjd" id="7DEtAv3qjIX" role="2Oq$k0">
                            <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
                          </node>
                          <node concept="3TrEf2" id="7DEtAv3qkwO" role="2OqNvi">
                            <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
                          </node>
                        </node>
                      </node>
                      <node concept="3TrEf2" id="7DEtAv3qm6S" role="2OqNvi">
                        <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdy" resolve="rule" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="7DEtAv3qjJ0" role="2OqNvi">
                      <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cai" resolve="leftHandSide" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="7DEtAv3qjJ1" role="2OqNvi">
                    <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                  </node>
                </node>
                <node concept="2OqwBi" id="7DEtAv3xIyl" role="3uHU7w">
                  <node concept="1YBJjd" id="7DEtAv3xIeF" role="2Oq$k0">
                    <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
                  </node>
                  <node concept="2qgKlT" id="7DEtAv3xIWi" role="2OqNvi">
                    <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cpWs8" id="7DEtAv3l_bY" role="3cqZAp">
        <node concept="3cpWsn" id="7DEtAv3l_bZ" role="3cpWs9">
          <property role="TrG5h" value="fromConsequence" />
          <node concept="_YKpA" id="7DEtAv3l$vD" role="1tU5fm">
            <node concept="3Tqbb2" id="7DEtAv3l$vG" role="_ZDj9">
              <ref role="ehGHo" to="c8s6:4vv0wgO7VEI" resolve="SymbolDeclaration" />
            </node>
          </node>
          <node concept="2OqwBi" id="7DEtAv3r8QX" role="33vP2m">
            <node concept="2OqwBi" id="7DEtAv3r8lJ" role="2Oq$k0">
              <node concept="1YBJjd" id="7DEtAv3r7VP" role="2Oq$k0">
                <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
              </node>
              <node concept="3TrEf2" id="7DEtAv3r8_6" role="2OqNvi">
                <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdb" resolve="fromPattern" />
              </node>
            </node>
            <node concept="2qgKlT" id="7DEtAv3r9d3" role="2OqNvi">
              <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
              <node concept="2OqwBi" id="7DEtAv3xJFv" role="37wK5m">
                <node concept="1YBJjd" id="7DEtAv3xJFw" role="2Oq$k0">
                  <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
                </node>
                <node concept="2qgKlT" id="7DEtAv3xJFx" role="2OqNvi">
                  <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3cpWs8" id="7DEtAv3l_Yo" role="3cqZAp">
        <node concept="3cpWsn" id="7DEtAv3l_Yp" role="3cpWs9">
          <property role="TrG5h" value="toConsequence" />
          <node concept="_YKpA" id="7DEtAv3l_Xg" role="1tU5fm">
            <node concept="3Tqbb2" id="7DEtAv3l_Xj" role="_ZDj9">
              <ref role="ehGHo" to="c8s6:4vv0wgO7VEI" resolve="SymbolDeclaration" />
            </node>
          </node>
          <node concept="2OqwBi" id="7DEtAv3z$Nn" role="33vP2m">
            <node concept="2OqwBi" id="7DEtAv3l_Yu" role="2Oq$k0">
              <node concept="1YBJjd" id="7DEtAv3l_Yv" role="2Oq$k0">
                <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
              </node>
              <node concept="3TrEf2" id="7DEtAv3z$vj" role="2OqNvi">
                <ref role="3Tt5mk" to="c8s6:7DEtAv3ysTw" resolve="toPattern" />
              </node>
            </node>
            <node concept="2qgKlT" id="7DEtAv3z_4z" role="2OqNvi">
              <ref role="37wK5l" to="h9ya:7DEtAv3qu1D" resolve="getConsequence" />
              <node concept="2OqwBi" id="7DEtAv3z_UJ" role="37wK5m">
                <node concept="1YBJjd" id="7DEtAv3z__T" role="2Oq$k0">
                  <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
                </node>
                <node concept="2qgKlT" id="7DEtAv3zAac" role="2OqNvi">
                  <ref role="37wK5l" to="h9ya:7DEtAv3xC4_" resolve="getSymbol" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="7DEtAv3lOLu" role="3cqZAp">
        <node concept="3clFbS" id="7DEtAv3lOLw" role="3clFbx">
          <node concept="2Mj0R9" id="7DEtAv3lAQs" role="3cqZAp">
            <node concept="17R0WA" id="7DEtAv3lU$k" role="2MkoU_">
              <node concept="37vLTw" id="7DEtAv3lUDW" role="3uHU7w">
                <ref role="3cqZAo" node="7DEtAv3l_bZ" resolve="fromConsequence" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3n2Pi" role="3uHU7B">
                <node concept="2OqwBi" id="7DEtAv3lL8S" role="2Oq$k0">
                  <node concept="37vLTw" id="7DEtAv3lKdy" role="2Oq$k0">
                    <ref role="3cqZAo" node="7DEtAv3l_Yp" resolve="toConsequence" />
                  </node>
                  <node concept="7r0gD" id="7DEtAv3lUch" role="2OqNvi">
                    <node concept="3cmrfG" id="7DEtAv3lUjN" role="7T0AP">
                      <property role="3cmrfH" value="1" />
                    </node>
                  </node>
                </node>
                <node concept="ANE8D" id="7DEtAv3n2Xq" role="2OqNvi" />
              </node>
            </node>
            <node concept="1YBJjd" id="7DEtAv3lVCl" role="2OEOjV">
              <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
            </node>
            <node concept="3cpWs3" id="7DEtAv3nAO9" role="2MkJ7o">
              <node concept="37vLTw" id="7DEtAv3nBOI" role="3uHU7w">
                <ref role="3cqZAo" node="7DEtAv3l_Yp" resolve="toConsequence" />
              </node>
              <node concept="3cpWs3" id="7DEtAv3nBCv" role="3uHU7B">
                <node concept="Xl_RD" id="7DEtAv3nBDq" role="3uHU7w">
                  <property role="Xl_RC" value=" =&gt; " />
                </node>
                <node concept="3cpWs3" id="7DEtAv3nAOb" role="3uHU7B">
                  <node concept="Xl_RD" id="7DEtAv3nAOc" role="3uHU7B">
                    <property role="Xl_RC" value="Transformation should just add one symbol: " />
                  </node>
                  <node concept="37vLTw" id="7DEtAv3nBHC" role="3uHU7w">
                    <ref role="3cqZAo" node="7DEtAv3l_bZ" resolve="fromConsequence" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="7DEtAv3lPOS" role="3clFbw">
          <node concept="2OqwBi" id="7DEtAv3lOWb" role="2Oq$k0">
            <node concept="1YBJjd" id="7DEtAv3lONi" role="2Oq$k0">
              <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
            </node>
            <node concept="3TrcHB" id="7DEtAv3lP_0" role="2OqNvi">
              <ref role="3TsBF5" to="c8s6:7DEtAv3lMVa" resolve="side" />
            </node>
          </node>
          <node concept="21noJN" id="7DEtAv3lQ8u" role="2OqNvi">
            <node concept="21nZrQ" id="7DEtAv3lQ8G" role="21noJM">
              <ref role="21nZrZ" to="tpc2:3Ftr4R6BFey" resolve="LEFT" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="7DEtAv3lVIM" role="3cqZAp">
        <node concept="3clFbS" id="7DEtAv3lVIN" role="3clFbx">
          <node concept="2Mj0R9" id="7DEtAv3lVIO" role="3cqZAp">
            <node concept="17R0WA" id="7DEtAv3lVIP" role="2MkoU_">
              <node concept="37vLTw" id="7DEtAv3lVIQ" role="3uHU7w">
                <ref role="3cqZAo" node="7DEtAv3l_bZ" resolve="fromConsequence" />
              </node>
              <node concept="2OqwBi" id="7DEtAv3n2sN" role="3uHU7B">
                <node concept="2OqwBi" id="7DEtAv3lVIR" role="2Oq$k0">
                  <node concept="37vLTw" id="7DEtAv3lVIS" role="2Oq$k0">
                    <ref role="3cqZAo" node="7DEtAv3l_Yp" resolve="toConsequence" />
                  </node>
                  <node concept="8ftyA" id="7DEtAv3lXb1" role="2OqNvi">
                    <node concept="3cpWsd" id="7DEtAv3m0DA" role="8f$Dv">
                      <node concept="3cmrfG" id="7DEtAv3m0DD" role="3uHU7w">
                        <property role="3cmrfH" value="1" />
                      </node>
                      <node concept="2OqwBi" id="7DEtAv3lZcG" role="3uHU7B">
                        <node concept="37vLTw" id="7DEtAv3lXhr" role="2Oq$k0">
                          <ref role="3cqZAo" node="7DEtAv3l_Yp" resolve="toConsequence" />
                        </node>
                        <node concept="34oBXx" id="7DEtAv3lZpz" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="ANE8D" id="7DEtAv3n2JR" role="2OqNvi" />
              </node>
            </node>
            <node concept="1YBJjd" id="7DEtAv3lVIW" role="2OEOjV">
              <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
            </node>
            <node concept="3cpWs3" id="7DEtAv3nBPV" role="2MkJ7o">
              <node concept="37vLTw" id="7DEtAv3nBPW" role="3uHU7w">
                <ref role="3cqZAo" node="7DEtAv3l_Yp" resolve="toConsequence" />
              </node>
              <node concept="3cpWs3" id="7DEtAv3nBPX" role="3uHU7B">
                <node concept="Xl_RD" id="7DEtAv3nBPY" role="3uHU7w">
                  <property role="Xl_RC" value=" =&gt; " />
                </node>
                <node concept="3cpWs3" id="7DEtAv3nBPZ" role="3uHU7B">
                  <node concept="Xl_RD" id="7DEtAv3nBQ0" role="3uHU7B">
                    <property role="Xl_RC" value="Transformation should just add one symbol: " />
                  </node>
                  <node concept="37vLTw" id="7DEtAv3nBQ1" role="3uHU7w">
                    <ref role="3cqZAo" node="7DEtAv3l_bZ" resolve="fromConsequence" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="7DEtAv3lVIX" role="3clFbw">
          <node concept="2OqwBi" id="7DEtAv3lVIY" role="2Oq$k0">
            <node concept="1YBJjd" id="7DEtAv3lVIZ" role="2Oq$k0">
              <ref role="1YBMHb" node="7DEtAv3lfz8" resolve="transform" />
            </node>
            <node concept="3TrcHB" id="7DEtAv3lVJ0" role="2OqNvi">
              <ref role="3TsBF5" to="c8s6:7DEtAv3lMVa" resolve="side" />
            </node>
          </node>
          <node concept="21noJN" id="7DEtAv3lVJ1" role="2OqNvi">
            <node concept="21nZrQ" id="7DEtAv3lVP9" role="21noJM">
              <ref role="21nZrZ" to="tpc2:3Ftr4R6BFex" resolve="RIGHT" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7DEtAv3lfz8" role="1YuTPh">
      <property role="TrG5h" value="transform" />
      <ref role="1YaFvo" to="c8s6:3v$U6ONMnP1" resolve="Transform" />
    </node>
  </node>
  <node concept="18kY7G" id="7DEtAv3px2k">
    <property role="TrG5h" value="check_TransformPatternRule" />
    <property role="3GE5qa" value="transform" />
    <node concept="3clFbS" id="7DEtAv3px2l" role="18ibNy">
      <node concept="2Mj0R9" id="7DEtAv3px2r" role="3cqZAp">
        <node concept="3clFbC" id="7DEtAv3pAoG" role="2MkoU_">
          <node concept="2OqwBi" id="7DEtAv3pF_P" role="3uHU7B">
            <node concept="2OqwBi" id="7DEtAv3pF_Q" role="2Oq$k0">
              <node concept="1YBJjd" id="7DEtAv3pF_R" role="2Oq$k0">
                <ref role="1YBMHb" node="7DEtAv3px2n" resolve="transformPattern" />
              </node>
              <node concept="3Tsc0h" id="7DEtAv3pF_S" role="2OqNvi">
                <ref role="3TtcxE" to="c8s6:7DEtAv3oLd$" resolve="childPatterns" />
              </node>
            </node>
            <node concept="34oBXx" id="7DEtAv3pF_T" role="2OqNvi" />
          </node>
          <node concept="2OqwBi" id="7DEtAv3pF_U" role="3uHU7w">
            <node concept="2OqwBi" id="7DEtAv3pF_V" role="2Oq$k0">
              <node concept="2OqwBi" id="7DEtAv3pF_W" role="2Oq$k0">
                <node concept="1YBJjd" id="7DEtAv3pF_X" role="2Oq$k0">
                  <ref role="1YBMHb" node="7DEtAv3px2n" resolve="transformPattern" />
                </node>
                <node concept="3TrEf2" id="7DEtAv3pF_Y" role="2OqNvi">
                  <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdy" resolve="rule" />
                </node>
              </node>
              <node concept="3Tsc0h" id="7DEtAv3pF_Z" role="2OqNvi">
                <ref role="3TtcxE" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
              </node>
            </node>
            <node concept="34oBXx" id="7DEtAv3pFA0" role="2OqNvi" />
          </node>
        </node>
        <node concept="Xl_RD" id="7DEtAv3pEYN" role="2MkJ7o">
          <property role="Xl_RC" value="Wrong argument size" />
        </node>
        <node concept="1YBJjd" id="7DEtAv3pF1_" role="2OEOjV">
          <ref role="1YBMHb" node="7DEtAv3px2n" resolve="transformPattern" />
        </node>
      </node>
      <node concept="1_o_46" id="7DEtAv3pFzU" role="3cqZAp">
        <node concept="1_o_bx" id="7DEtAv3pFCR" role="1_o_by">
          <node concept="1_o_bG" id="7DEtAv3pFCS" role="1_o_aQ">
            <property role="TrG5h" value="childPattern" />
          </node>
          <node concept="2OqwBi" id="7DEtAv3pBbP" role="1_o_bz">
            <node concept="1YBJjd" id="7DEtAv3pAO_" role="2Oq$k0">
              <ref role="1YBMHb" node="7DEtAv3px2n" resolve="transformPattern" />
            </node>
            <node concept="3Tsc0h" id="7DEtAv3pBNp" role="2OqNvi">
              <ref role="3TtcxE" to="c8s6:7DEtAv3oLd$" resolve="childPatterns" />
            </node>
          </node>
        </node>
        <node concept="1_o_bx" id="7DEtAv3pFzW" role="1_o_by">
          <node concept="1_o_bG" id="7DEtAv3pFzY" role="1_o_aQ">
            <property role="TrG5h" value="consequenceElem" />
          </node>
          <node concept="2OqwBi" id="7DEtAv3pF$H" role="1_o_bz">
            <node concept="2OqwBi" id="7DEtAv3pF$I" role="2Oq$k0">
              <node concept="1YBJjd" id="7DEtAv3pF$J" role="2Oq$k0">
                <ref role="1YBMHb" node="7DEtAv3px2n" resolve="transformPattern" />
              </node>
              <node concept="3TrEf2" id="7DEtAv3pF$K" role="2OqNvi">
                <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdy" resolve="rule" />
              </node>
            </node>
            <node concept="3Tsc0h" id="7DEtAv3pF$L" role="2OqNvi">
              <ref role="3TtcxE" to="c8s6:4vv0wgO7Cak" resolve="rightHandSide" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="7DEtAv3pF$2" role="2LFqv$">
          <node concept="1_3QMa" id="7DEtAv3pHQ4" role="3cqZAp">
            <node concept="1pnPoh" id="7DEtAv3pIks" role="1_3QMm">
              <node concept="3gn64h" id="7DEtAv3pIk_" role="1pnPq6">
                <ref role="3gnhBz" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
              </node>
              <node concept="3clFbS" id="7DEtAv3pIkw" role="1pnPq1">
                <node concept="2Mj0R9" id="7DEtAv3pGjH" role="3cqZAp">
                  <node concept="2OqwBi" id="7DEtAv3pGt0" role="2MkoU_">
                    <node concept="3M$PaV" id="7DEtAv3pHa8" role="2Oq$k0">
                      <ref role="3M$S_o" node="7DEtAv3pFCS" resolve="childPattern" />
                    </node>
                    <node concept="1mIQ4w" id="7DEtAv3pHrf" role="2OqNvi">
                      <node concept="chp4Y" id="7DEtAv3pHtp" role="cj9EA">
                        <ref role="cht4Q" to="c8s6:7DEtAv3oLdB" resolve="TransformPatternAny" />
                      </node>
                    </node>
                  </node>
                  <node concept="Xl_RD" id="7DEtAv3pHwr" role="2MkJ7o">
                    <property role="Xl_RC" value="Child pattern cannot be defined for terminal" />
                  </node>
                  <node concept="3M$PaV" id="7DEtAv3pH$Q" role="2OEOjV">
                    <ref role="3M$S_o" node="7DEtAv3pFCS" resolve="childPattern" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1pnPoh" id="7DEtAv3pIkH" role="1_3QMm">
              <node concept="3gn64h" id="7DEtAv3pIkT" role="1pnPq6">
                <ref role="3gnhBz" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
              </node>
              <node concept="3clFbS" id="7DEtAv3pIkL" role="1pnPq1">
                <node concept="1_3QMa" id="7DEtAv3qsSp" role="3cqZAp">
                  <node concept="1_3QMl" id="7DEtAv3qtfL" role="1_3QMm">
                    <node concept="3gn64h" id="7DEtAv3qtfU" role="3Kbmr1">
                      <ref role="3gnhBz" to="c8s6:7DEtAv3oLdr" resolve="TransformPatternRule" />
                    </node>
                    <node concept="3clFbS" id="7DEtAv3qtfP" role="3Kbo56">
                      <node concept="2Mj0R9" id="7DEtAv3pOkg" role="3cqZAp">
                        <node concept="Xl_RD" id="7DEtAv3pOyC" role="2MkJ7o">
                          <property role="Xl_RC" value="Rule mismatches" />
                        </node>
                        <node concept="3M$PaV" id="7DEtAv3pOz5" role="2OEOjV">
                          <ref role="3M$S_o" node="7DEtAv3pFCS" resolve="childPattern" />
                        </node>
                        <node concept="3clFbC" id="7DEtAv3pMsb" role="2MkoU_">
                          <node concept="2OqwBi" id="7DEtAv3pNO3" role="3uHU7B">
                            <node concept="1PxgMI" id="7DEtAv3pNO4" role="2Oq$k0">
                              <node concept="chp4Y" id="7DEtAv3pNO5" role="3oSUPX">
                                <ref role="cht4Q" to="c8s6:4vv0wgO7Caf" resolve="NonterminalReference" />
                              </node>
                              <node concept="3M$PaV" id="7DEtAv3pNO6" role="1m5AlR">
                                <ref role="3M$S_o" node="7DEtAv3pFzY" resolve="consequenceElem" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="7DEtAv3pNO7" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="7DEtAv3pNO8" role="3uHU7w">
                            <node concept="2OqwBi" id="7DEtAv3pNO9" role="2Oq$k0">
                              <node concept="2OqwBi" id="7DEtAv3pNOa" role="2Oq$k0">
                                <node concept="1PxgMI" id="7DEtAv3pNOb" role="2Oq$k0">
                                  <node concept="chp4Y" id="7DEtAv3pNOc" role="3oSUPX">
                                    <ref role="cht4Q" to="c8s6:7DEtAv3oLdr" resolve="TransformPatternRule" />
                                  </node>
                                  <node concept="3M$PaV" id="7DEtAv3pNOd" role="1m5AlR">
                                    <ref role="3M$S_o" node="7DEtAv3pFCS" resolve="childPattern" />
                                  </node>
                                </node>
                                <node concept="3TrEf2" id="7DEtAv3pNOe" role="2OqNvi">
                                  <ref role="3Tt5mk" to="c8s6:7DEtAv3oLdy" resolve="rule" />
                                </node>
                              </node>
                              <node concept="3TrEf2" id="7DEtAv3pNOf" role="2OqNvi">
                                <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cai" resolve="leftHandSide" />
                              </node>
                            </node>
                            <node concept="3TrEf2" id="7DEtAv3pNOg" role="2OqNvi">
                              <ref role="3Tt5mk" to="c8s6:4vv0wgO7Cag" resolve="declaration" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="7DEtAv3qt03" role="1_3QMn">
                    <node concept="3M$PaV" id="7DEtAv3qsSR" role="2Oq$k0">
                      <ref role="3M$S_o" node="7DEtAv3pFCS" resolve="childPattern" />
                    </node>
                    <node concept="2yIwOk" id="7DEtAv3qtfy" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="7DEtAv3pHXA" role="1_3QMn">
              <node concept="3M$PaV" id="7DEtAv3pHQq" role="2Oq$k0">
                <ref role="3M$S_o" node="7DEtAv3pFzY" resolve="consequenceElem" />
              </node>
              <node concept="2yIwOk" id="7DEtAv3pIk3" role="2OqNvi" />
            </node>
          </node>
          <node concept="3clFbJ" id="7DEtAv3pFM5" role="3cqZAp">
            <node concept="2OqwBi" id="7DEtAv3pFVa" role="3clFbw">
              <node concept="3M$PaV" id="7DEtAv3pGFx" role="2Oq$k0">
                <ref role="3M$S_o" node="7DEtAv3pFzY" resolve="consequenceElem" />
              </node>
              <node concept="1mIQ4w" id="7DEtAv3pG4L" role="2OqNvi">
                <node concept="chp4Y" id="7DEtAv3pGT1" role="cj9EA">
                  <ref role="cht4Q" to="c8s6:4vv0wgO7Cac" resolve="TerminalReference" />
                </node>
              </node>
            </node>
            <node concept="3clFbS" id="7DEtAv3pFM7" role="3clFbx" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7DEtAv3px2n" role="1YuTPh">
      <property role="TrG5h" value="transformPattern" />
      <ref role="1YaFvo" to="c8s6:7DEtAv3oLdr" resolve="TransformPatternRule" />
    </node>
  </node>
</model>

